﻿Module Module1

    Sub Main()
        Dim x, y, z As Double
        Dim Opcion As Char = ""
        While Opcion <> "z"
            Console.Clear()
            Console.WriteLine("Opcion Suma......(s)")
            Console.WriteLine("Opcion Resta......(r)")
            Console.WriteLine("Opcion Multiplicacion......(m)")
            Console.WriteLine("Opcion Division......(d)")
            Console.WriteLine("Opcion Salir......(z)")
            Try
                Opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case Opcion
                    Case "s"
                        Console.WriteLine("Opcion seleccionada fue suma")
                        Console.WriteLine("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.WriteLine("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x + y
                        Console.WriteLine("Resultado de la suma es:" & z)
                    Case "r"
                        Console.WriteLine("Opcion seleccionada fue resta")
                        Console.WriteLine("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.WriteLine("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x - y
                        Console.WriteLine("Resultado de la resta es:" & z)
                    Case "m"
                        Console.WriteLine("Opcion seleccionada fue multiplicacion")
                        Console.WriteLine("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.WriteLine("Ingrese segundo numero")
                        y = Console.ReadLine()
                        z = x * y
                        Console.WriteLine("Resultado de la multiplicacion es:" & z)
                    Case "d"
                        Console.WriteLine("Opcion seleccionada fue division")
                        Console.WriteLine("Ingrese primer numero")
                        x = Console.ReadLine()
                        Console.WriteLine("Ingrese segundo numero")
                        y = Console.ReadLine()
                        If y <> 0 Then
                            z = x / y
                            Console.WriteLine("Resultado de la divion es:" & z)
                        ElseIf y = 0 Then
                            Console.WriteLine("Resultado de la divion entre cero no es posible")
                        End If
                    Case "z"
                        Console.Clear()
                        Console.WriteLine("Gracias por utilizar nuestra plataforma")
                    Case Else
                        Console.Clear()
                        Console.WriteLine("Opcion ingresada no es valida, vuelva a intentar")

                End Select
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
            If Opcion <> "z" Then
                Console.WriteLine("Pulse cualquier teclara para regresar al menu")
                Console.ReadKey()
            End If
        End While
    End Sub

End Module
